import React from "react";

const App: React.FunctionComponent = () => <h1>Welcome @ leancode bootcamp!</h1>;

export default App;
