FROM nginx:alpine

COPY ./release/nginx.web.conf /etc/nginx/conf.d/default.conf

COPY deploy /usr/share/nginx/html

ENTRYPOINT nginx -g "daemon off;"
