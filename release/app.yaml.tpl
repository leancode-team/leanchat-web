apiVersion: apps/v1
kind: Deployment
metadata:
  name: leanchat-web-${APP_NAME}
  namespace: leanchat
  labels:
    app: leanchat
    component: web
    name: ${APP_NAME}
spec:
  selector:
    matchLabels:
      app: leanchat
      component: web
      name: ${APP_NAME}
  replicas: 1
  revisionHistoryLimit: 3
  template:
    metadata:
      labels:
        app: leanchat
        component: web
        name: ${APP_NAME}
    spec:
      containers:
      - name: web
        image: leancode.azurecr.io/leanchat-web-${APP_NAME}:${APP_VERSION}
        ports:
        - containerPort: 80
        livenessProbe:
          httpGet:
            path: /
            port: 80
          initialDelaySeconds: 10
          periodSeconds: 5
        readinessProbe:
          httpGet:
            path: /
            port: 80
          initialDelaySeconds: 10
          periodSeconds: 10
---
apiVersion: v1
kind: Service
metadata:
  name: leanchat-web-${APP_NAME}
  namespace: leanchat
  labels:
    app: leanchat
    component: web
    name: ${APP_NAME}
spec:
  type: ClusterIP
  ports:
  - port: 80
  selector:
    app: leanchat
    component: web
    name: ${APP_NAME}
---
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: leanchat-web-${APP_NAME}-ingress
  namespace: leanchat
  labels:
    app: leanchat
    component: web
    name: ${APP_NAME}
spec:
  rules:
  - host: ${APP_NAME}.${DOMAIN}
    http:
      paths:
      - backend:
          serviceName: leanchat-web-${APP_NAME}
          servicePort: 80
