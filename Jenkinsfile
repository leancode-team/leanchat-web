def currentVersion = "0.1";

leancode.builder('leanchat-web')
    .withCustomJnlp()
    .withDocker()
    .withNode()
    .withDeployTools()
    .run {

    def scmVars

    stage('Checkout') {
        scmVars = safeCheckout scm
    }

    leancode.configureRepositories()

    stage('Version') {
        env.GIT_COMMIT = scmVars.GIT_COMMIT
        env.APP_NAME = "$BRANCH_NAME".replaceAll("/", "").replaceAll("-", "");
        env.APP_VERSION = "${currentVersion}.${nextBuildNumber()}"
        env.BLUEBIRD_DEBUG = 0 // https://npm.community/t/npm-ci-produces-a-tonn-of-excess-warnings-on-install/3261
        echo "Building version: ${env.APP_NAME} ${env.APP_VERSION}"

        currentBuild.displayName = "${env.APP_NAME}-${env.APP_VERSION}"
    }

    container('node') {
        stage('Restore') {
            sh 'NODE_ENV=development npm ci'
        }

        stage('Build') {
            env.DOTENV_PATH = "release/prod.env"
            sh 'NODE_ENV=production npm run build'
        }
    }

    stage('Push containers') {
        leancode.withACR {
            def web = docker.build(
                "${leancode.ACR()}/leanchat-web-${env.APP_NAME}:$APP_VERSION",
                '-f release/web.dockerfile .')
            web.push()
        }
    }

    dir('release') {
        stage('Deploy to test') {
            container('deploy-tools') {
                env.DOMAIN = 'leanchat.aks.lncd.pl'
                sh 'envsubst < app.yaml.tpl > app.yaml'
                sh 'kubectl apply -f app.yaml'

                archiveArtifacts artifacts: '*.yaml', fingerprint: true
                relatedBuild.writeAsArtifact "$APP_VERSION", 'APP_VERSION'
                relatedBuild.writeAsArtifact "$APP_NAME", 'APP_NAME'
            }
        }
    }
}
